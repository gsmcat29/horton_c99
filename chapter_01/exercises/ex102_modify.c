/* Exercise 1-2. Modify your solution for the previous exercise so that it 
produces all the output using only one printf() statement.*/
#include <stdio.h>

int main()
{
    printf("My name:    John Doe\nMy address: Viscount City Boulevard 9999, \
    El Paso TX\n");


    return 0;
}