/* Exercise 1-1. Write a program that will output your name and address using a 
separate printf() statement for each line of output.*/

#include <stdio.h>

int main()
{
    printf("My name:    John Doe\n");
    printf("My address: Viscount City Boulevard 9999, El Paso TX\n");

    return 0;
}